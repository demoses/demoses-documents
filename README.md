# DEMOSES-Documents

This repository contains a number of working documents that serve as documentation and reference material for the DEMOSES project

The latest rendered PDF versions of all documents in this repository can be downloaded as a zip file from the [artifacts](https://gitlab.tudelft.nl/demoses/demoses-documents/-/artifacts) page.

## Documents

- **docs/Coupling-with-Tulipa.md**

  Details involved with the potential use of Tulipa as the central model in the proposed model coupling framework.
- **docs/architecture-description.md**

  A high-level description of the software/model coupling architecture for workpackages 2 & 3.
- **docs/functional-requirements-satellite-models.md**

  A detailed description of assumptions to take into account, and functional requirements to adhere to for any satellite model to successfully couple with the central model.

- **docs/glossary.md**

  A collection of terms and their definitions as used within the DEMOSES project

## Maintainers:

1. Sander van Rijn
1. Christian Doh Dinga
1. Sugandha Chauhan

