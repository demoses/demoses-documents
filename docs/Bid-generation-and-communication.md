# Bid generation and communication in model coupling framework
##  Simplified bid generation - protoype for model interface - 1 MW load with 48 hour flexibility
1. Take 48 hour price forecast
2. The consumer needs to fulfil a 1 MW load in any hour in a 48 hour time period
3. The bid-generation function first identifies the lowest price hour. If that hour is in the first 24 hours, it allocates the entire demand to that hour and bids at that price. If that hour is in the 25-48 hr period, the model generates 1 bid at the lowest price and submits it during the lowest price hour of day-1. In case that bid is not accepted, then, in the next 24-hr period, the model submits the bid at a higher price in the lowest price hour.
![Generating simplified bid using 48 hr flexible load](../img/simplified_bid.png)

## Complex bid generation - what actually will be used in the papers
1. A price forecast of n hours is provided. Each consumer gets the same forecast?
2. A level of noise is added to the forecast to simulate uncertainty. 5-6 such profiles are generated for each consumer representing a range of uncertainty of the optimisation horizon of that consumer. 
3. Each consumer performs its internal optimisation for each of the price profiles generated and outputs demand levels corresponding to each price in each profile. That is, one demand profile per price profile. These sets of price-volume will be used to generate bids.
![Price-demand profiles](../img/price-demand-profile.png)
4. Generating bids from price-demand profiles

|Price profile | Price | Demand | Bid price | Bid Volume |
|--------------|-------|--------|-----------|------------|
|Profile 1     | P1    | Q1     | P1        | Q1         |
|Profile 2     | P2    | Q2     | P2        | Q2 - Q1    |
|Profile 3     | P3    | Q3     | P3        | Q3 - Q2    |
|Profile 4     | P4    | Q4     | P4        | Q4 - Q3    |

![Bids for timestep t1](../img/bids.png)

Constraints: $\sum_{t=1}^{24} Q4 <= Q_D^{max} \quad \forall \, D \in days$


