## Introduction

Using Tulipa as the central model in DEMOSES will require an exchange of bidding information with the partner models. Bidding information includes:
- Bid pair - volume and price, time period, block period, etc.
- Bid type - single hour or block bids

Essentially, the model will treat demand bids as negative generation with a marginal cost associated with each quantity. Similar to generators, different parameters can be assigned to the bids such as ramp rates, minimum up and down times, etc. We can call these parameters the unit commitment (UC) parameters for flexible demand. Due to the varying nature of the bids, different bids will be treated differently by the model and that is why it is essential to specify the type of bid at the model coupling interface. Below, we discuss the way bids can be incorported in Tulipa (based on bid types stated in the [EPEX Trading Brochure](https://www.epexspot.com/en/tradingproducts#day-ahead-trading)).

## Bid Types

There are two main types of bids:
- Single hour bids: a consumer can submit up to 256 bid pairs for each time period. Bids submitted in different time periods are not linked to each other. There have to be volumes submitted at both the ceiling and the floor bid price (they can be zero too). These bids can be entered directly without any associated constraints or unit commitment parameters in the Tulipa model.
- Block bids - These bids are linked to each other across multiple timesteps and their clearing is conditional based on the acceptance of the entire or part of the block as determined by the bid type. Block bids will require mixed-integer formulation since there are startup (first bid acceptance), minimum up time (number of time periods) and other constraints that require binary variables. It must be noted that in the case of a block bid, there will be only one bid-pair per time-period per block. If there are multiple blocks then the same time-period can have different bid pairs but they will be associated with the different blocks and they cannot all be cleared together. Only one of them and their subsequent time-period bids will be cleared. 

### Block Bid Types
**NOTE** The following formulations are not completely correct, they merely serve as a partial/limited illustration.
_A more complete mathematical definition of EPEX bids might be found in [this paper](https://www.sciencedirect.com/science/article/pii/S0377221714007991)._

There can be three types of block bids:

- Simple block bids - These are blocks of the same volume over consecutive time periods. They can be fill-or-kill blocks (meaning either all volume in all time periods gets cleared) or curtailable blocks (meaning a certain % $\phi \in (0, 1]$ of bid volume can be unmet over the block time periods). In MILP terms, a fill-or-kill block can be formulated using a shared decision variable $y \in {0, 1}$ (yes/no) as
  $$ B^{t=1...n} = \{b_1, b_2, ..., b_n\} = \{(p_1, q_1 \cdot y), (p_2, q_2 \cdot y), ..., (p_n, q_n \cdot y)\} $$
  where $q_1 = q_2 = ... = q_n$. Note that $q_1, ... q_n$ in this case are not decision variables, but are fixed values instead: the solver can only choose to enable or disable the entire block.

  A curtailable block includes the additional $r$ term to a bid $(p_t, q_t \cdot y \cdot r)$ where $r \in [\phi, 1]$

- Profiled blocks - these can have different bid volumes across different (but still consecutive) time periods. The entire block needs to be cleared as one bid across all the time periods. This can account for variation in operational requirements.
  For the MILP formulation, this implies dropping the constraint that $q_1 = q_2 = ... = q_n$.

- Smart and Big (SAB) blocks
  - Linked blocks (C02) - these are parent-child blocks where the clearance of one bid (child) is dependent on the clearance of another bid (parent). There can be up to 7 blocks in a linked family. Thus, there can be 7 generations. In each generation there will be only 1 parents with up to 6 children. Children are not allowed to have more than 1 parent. This provides a lot of flexibility to the consumer to meet its demand under different operational configurations of its devices (or technology). This will be of relevance to consumers like Tata Steel who have both ramping and minimum up/down time requirements.
  - Exclusive blocks (C04) - in this, only one block in a group of blocks bids can be accepted. It allows a consumer to take advantage of the lowest possible market price. Thus, the block will only be accepted where the consumer surplus (or negative cost) is the highest (or lowest).

    E.g., for an exclusive block $E = \{B_1, B_2, ..., B_n\}$, each with associated shared decision variable $y_1, y_2, ..., y_n$, this adds the constraint that $1 \geq \sum^n_{i=1} y_i$.

  - Loop blocks (C88) - these blocks are for the specific use for storage devices. These are linked blocks of buy and sell which are interdependent. Thus, a consumer can only buy(sell) power it is also able to sell(buy) it within the same market horizon (day-ahead).
  - Big blocks - These blocks can be up to 1500 MW in size. Typical blocks can have up to 600 MW size.

