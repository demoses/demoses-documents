# DEMOSES Model coupling architecture using Willingnes-to-Pay (WTP) information exchange

Authors: Christian Doh Dinga, Sander van Rijn


## Goal

Energy systems are changing. Demand used to be mostly known in advance, and supply from coal and gas generators could be flexibly scheduled. Now supply from renewable sources such as wind and solar are uncontrollable and can only partially be predicted. As large scale and long term energy storage is still very expensive, the modern energy grid will have to make optimal use of all available flexibility on the demand side.

A traditional approach for scheduling flexibility in integrated energy systems is by using co-optimization, i.e., optimizing all parts simultaneously in a single system. However, co-optimization has several drawbacks such as:

1. It requires complete information regarding the internal state of all flexible consumers in the integrated energy system. However, some flexible consumers might not be willing to share such information due to confidentiality reasons.
2. It assumes a central system operator has full control over all flexibility assets of the integrated energy system and dispatches them to minimize system cost. In reality, these assets are owned/operated by other system operators who seek to minimize their own operational costs and not necessarily that of the entire energy system.
3. This approach is computationally expensive and does not scale well if all sub-systems are modeled in high temporal, spatial, and techno-economic resolution. Therefore, it usually makes significant simplifications.

The main goal of this software is to provide a fast and scalable approach for (near) cost-optimally scheduling flexibility in an integrated energy system with limited information (flexible consumer confidentiality). The ultimate goal for scheduling flexibility is for:

1. Maximizing social welfare, defined as production cost (marginal cost * generator dispatch) - gross surplus (demand met * bid price)
2. Efficient integration of wind and solar into the energy system
3. Congestion management in the electricity distribution network


### Scope

The scope of this architecture is currently two-fold:

- It will be used to model a coupling to the heat sector of South-Holland with a focus on congestion management (WP2). Time resolution of the model will likely be 1h or 15m.
- It will be used to model the interplay of market interventions and flexibility on the scale of the Netherlands, possibly with some import/export coupling (WP3). Time resolution will mimic the Dutch day-ahead market, so a 1h resolution with the market being cleared in blocks 24h.


## Iterative Price Response

To address especially the first drawback, we could perform an iterative coupled optimization using a central market model and dedicated satellite models. The satellite models each represent a consumer with some demand flexibility, and can output a time series of demand values when given a time series of market prices for a desired time horizon. The market model would accept all these partial demands, combine them, and perform system cost minimization as usual such that market prices can be read out after the optimization.

The iterations would be performed as follows:

0. Starting at iteration $i=0$, satellite models are initialized to report a time series of some (arbitrary) demand values ( $Q_{0}$ )
1. The central market model receives and combines demand time series from all satellite models
2. The central market model performs the unit commitment optimization with respect to minimizing total system cost
3. The central market model reports back with the resulting time series of market prices ( $P_i$ )
4. The satellite models receive a time series of market prices
5. The satellite models report back with a time series of demand values ( $Q_{i+1}$ )
6. If the market prices / demand time series are not equal to those in the previous iteration, continue from 1 and increase $i$ by 1.

An illustration of the first few steps of this process for single price and quantity values is shown below:

![Iterative Price Response: convergent example](../img/fixed_point_iterations_converging.png)

While this addresses drawback 1 and 2, the computational cost mentioned as drawback 3 is only likely to be worse since the whole time horizon optimization now has to be repeated several times. A new potential drawback is that the demand/price time series may not converge at all, or may end up in a limit cycle. A divergent example is shown below. Both these situations have to be detected, and will have to be addressed somehow, e.g. by re-running with different initialization.

![Iterative Price Response: divergent example](../img/fixed_point_iterations_diverging.png)

To address this computational complexity, we consider stepping away from the optimality of a full-horizon optimization, and look at a stepwise simulation approach instead.

## Proposed Approach

For flexible consumers participating in the task of modelling the flexibility of the energy system, we assume the only information they are willing to share is their **Willingness-to-Pay** (WTP), expressed as a sequence of (quantity, bid price) pairs. How they arrive there can be their confidential trade secret, encapsulated in separate **Satellite Models**. The shared WTP information can finally be gathered by a public **Central Model**, which performs the social welfare maximization.

This separation avoids the first and second drawbacks mentioned above: the satellite models do not have to share internal details and do not give up any control. A new drawback of this approach is that the flexibility is limited by the time resolution of the exchanged WTP information. After all, if WTP information has to be exchanged for multiple timesteps, there is no longer the option for a flexible consumer to change demand at time `t+1` based on the result at time `t` without providing extra information to the central model. This would break the assumption that WTP is the only sharable information.

The simplest way to avoid this new drawback is to perform the information exchange and central optimization for one timestep at a time. This means treating the problem as a simulation rather than a full optimization, since we cannot guarantee optimality if a flexible consumer would have made different choices in hindsight. Restricting the problem to one timestep at a time does greatly reduce the computational complexity, which addresses the third listed drawback of co-optimization.

Defining the components more strictly:

1. __Central Model__: The central model is an operational model of the power system that optimizes the dispatch of generation units to satisfy demand, which has some flexibility specified through a collection of WTP information. The central model can be as simple as a copper-plate economic dispatch model or a lossless DCOPF model with/without unit commitment.

2. __Satellite models__: Satellite models represent flexible power consumers such as Eneco, Tata Steel, EV aggregators, etc. These flexible consumers interact with the central model using WTP information.

3. __Willingness-to-Pay information:__ The interaction (information exchange) between the central model (power dispatch) and satellite models (flexible consumers) is done through WTP information. The WTP information of a flexible consumer shows how its electricity demand (quantity or volume) varies with price. It conveys information about how much power (quantity) the consumer is willing to consume at a given electricity price. Generally, there is an inverse relation between price and volume, as consumers will increase demand (volume) when prices are low and decrease demand when prices are high.

See below for a schematic example of the interaction between the satellite models exchanging WTP information with a central market model.

![Schematic view of satellite models exchanging WTP information](../img/schematic-wtp-exchange.png)

### Interaction Description

The model coupling and interaction procedure at timestep `t` is as follows:

1. Each satellite model updates its internal state
2. Each satellite model constructs their WTP information for timestep `t`
3. Each satellite model sends their WTP information to the central model
4. The central model collects WTP information from all satellite models
5. The central model performs the power dispatch optimization to determine the market price
6. The central model communicates the market price and the amount of satisfied demand to each satellite model
7. Time moves to timestep `t+1`, process repeats at step 1.

A satellite model can use whatever internal information they want to determine their WTP information. This will typically depend on some private internal state, which is updated at the start of every iteration (steps 1 & 2). For example, a large consumer like Tata Steel may have a significant base load and some flexible production that can be scheduled earlier or later, as long as the long-term production targets are met. The internal state could then be whether production is currently ahead or behind schedule. If production is ahead of schedule, the willingness to pay would be lower, as there is financial flexibility to wait for cheaper prices. If production is behind schedule though, the willingness to pay would be higher to avoid even larger costs for not reaching the production targets. It is the task of each satellite model to translate such decisions into bids according to the agreed upon exchange format. Note also that the runtime of this step should be limited to prevent the total simulation time from taking too long.

Once the central model has gathered all WTP information (steps 3 & 4), the combined demand can be installed as the demand part of the model. Then the power dispatch optimization can run (step 5) to determine the crossover point between the supply and demand. A market price can be determined from this result, which combined with the bids exchanged by the satellite models determines the amount of demand that was supplied to that flexible consumer. The market price and amount of demand met is fed back to the satellite models (step 6). Now both the satellite model and central model have taken their turn, the simulation moves to the next timestep and repeats the process until the end of the simulation period has been reached (step 7).


## Dynamic Programming for Dynamic Constraints (WP3)

In the day-ahead electricity market considered by WP3, there may be additional constraints that act conditional on the resulting commitment at prior timesteps. Examples of these can include supply-side minimum up/downtimes and ramp rates. Such constraints are difficult and/or expensive to model in a pure MILP formulation, and would require significant overhead in the per-timestep model coupling approach. They can however be approximated by a combination of the naive iterative approach and Dynamic Programming:

Beforehand: set up MILP problem as usual, without dynamic constraints.

1. Run (MI)LP to perform market clearing for given supply and demand values/functions
2. Obtain market price for each timestep
3. Run dynamic programming to (re-)allocate supply/demand: this can easily model the dynamic constraints such as ramp rate
4. Obtain zero-commitment results for assets at certain timesteps: dynamic programming will give the optimal 'route' given the dynamic constraints, including when certain suppliers or consumers are inactive.
5. Drop zero-committed assets from the MILP problem definition.
6. Repeat from 1. until convergence is reached.

The procedure above would traditionally replace step 4. in the iterative, naively coupled system described before. However, it can also be used as part of the per-timestep model coupling approach (i.e., replacing step 5), to only simulate in blocks of 24h without the full-horizon converging iterations. Since the problem is restricted to solving for a 24h time period, each iteration is expected to run very fast. Some guards will have to be implemented to detect and handle this new inner loop either diverging or the convergence getting stuck in a limit cycle.

