# Functional Requirements for DEMOSES Satellite Models

## Central Model Scope & Assumptions

The central model has two important properties
- Time resolution
- Interaction resolution

Here's a schematic look at how the satellite model should run:

> TODO: update figure to include optimization horizon???

    t_0         t_n         t_2n        t_3n
     (simulation)  (satellite model look-ahead)
I1   |-----------|- - -
I2               |-----------|- - -
I3                           |-----------|- - -
..                                       |---------

The central model will determine the *time resolution* `t`, e.g. 1h. At each interaction iteration, the satellite model should run to generate enough information to perform the information exchange. This amount is specified by the *interaction resolution* `i`, which is an integer multiple `n >= 1` of the time resolution `t`. As example: the `t` might be `1h`, and `i` can be `1*t = 1h`, as is the case for WP2. Another example could be that `t` is `1h`, but `i` is `24*t = 24h`, as was originally envisioned for WP3.

### To discuss: do we want to commit to implementing this?
Additionally, a *optimization horizon* (look-ahead period?) `o` can be defined on top of that, which would be an integer multiple `m >= 1` of the interaction resolution `i`. This optimization horizon would be used to perform price prediction for the future, which can be passed to the satellite models for them to use in their own predictions/simulations when determining their demand for the next timestep(s).


## Outputs from Satellite Model

- Bid pairs (price, quantity)
These have to be specified at the central model's time resolution `t`, and for the whole interaction resolution `i` (or optimization horizon `o` if we commit to that). They can consist of as many pairs per timestep as desired (although practical limits may have to be imposed in case of runtime constraints).


## Inputs to Satellite Model

- (Predicted) amount of demand met **and** market price (predictions)
After the central model has performed market clearing, the amount of demand that was met at each timestep of the interaction resolution is reported back to the satellite model, along with the associated market price at that timestep. Both are necessary to exactly specify the intersection of supply and demand, as shown in the example below:

---------               |
        |     __________|
        |    |
        -----+- - - - -
             |        |
  ___________|        |
                      ------------

Here, ---- and - - - are bids from two separate flexible consumers, which happen to share a bid price. As only part of their combined bids are met by the supply with the lower marginal cost, only reporting that as the resulting market price would lead each consumer to believe their whole bid was met. Similarly, only reporting that a proportion of that intersecting bid was met does not give information about at what market price that was done.

If the optimization horizon is implemented, then also the predicted amount of demand met and market prices will be reported to the satellite models for every timestep of both the interaction period and the additional timesteps in the optimization horizon. Whether the satellite models actually use *all* this information is up to them.


## WP2 Specification

For DEMOSES WP2, we specify the timestep `t` to be in 1h resolution, and the interaction resolution `i` to be 1h also. Whether or not to implement the optimization horizon `o` (of any length) is currently undecided (d.d. 2024-02-21).

