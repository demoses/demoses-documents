# Glossary

Collection of terms and definitions as used within the DEMOSES project
Elasticity: change in demand volume (quantity) with change in price

**Flexibility (of demand)** Collective term for temporally shifting or shedding demand

**Elasticity (of demand)** Response of demand as a function of price

